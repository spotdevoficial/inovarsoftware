/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import org.controlsfx.control.CheckListView;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.utils.GroupList.GroupEvent;

import facebook4j.FacebookException;
import facebook4j.Group;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.event.Event;
import javafx.scene.Cursor;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * @author Davi Salles
 * @description BorderPane que representa um item do
 * grupo na lista de grupos
 */
public class GroupItemPaneView extends BorderPane {

	/**
	 * Url da imagem do facebook 
	 */
	private String imageUrl;

	/**
	 * Titulo que ficará no ícone do grupo
	 */
	private String title;
	
	/**
	 * Pane que fica visivel apenas se item tiver sido selecionado
	 */
	private BorderPane isSelectedPane;
	
	/**
	 * Conta do facebook que participa do grupo
	 */
	private FacebookInovar facebookInovar;
	
	/**
	 * Pane onde fica a imagem do grupo
	 */
	private BorderPane picture;
	
	/**
	 * Pane de tamanho mínimo para o ícone do grupo
	 */
	private BorderPane minifiedPane;
	
	/**
	 * Grupo que o item representa
	 */
	private Group grupo;
	
	/**
	 * Lista popup com grupos do navbar
	 */
	private CheckListView<String> groupPopupList;
	
	/**
	 * Esse construtor deve ser usado quando o InovarSoftawre.DISABLE_FACEBOOK
	 *  é TRUE
	 * 
	 * @param imageUrl Url de uma imagem que ficara no ícone
	 * @param title Título do grupo do ícone
	 * @param facebookInovar Conta 
	 */
	public GroupItemPaneView(String imageUrl, String title){
		
		this.imageUrl = imageUrl;
		this.title = title;
		
		this.setCursor(Cursor.HAND);
		this.getStyleClass().add("button-item-group");
		this.setOnMousePressed(this::onClickGroupEvent);
		
		// Cria o título do grupo
		Text titulo = new Text(title);
		titulo.setWrappingWidth(110);
		titulo.setStyle("-fx-fill: white;");
		titulo.setTextAlignment(TextAlignment.CENTER);
		titulo.setFont(new Font(15));
		
		// Cria a imagem do grupo
		picture = new BorderPane();
		minifiedPane = new BorderPane();
		if(imageUrl != null) {
			picture.setStyle("-fx-background-image: url("+imageUrl+")");
			minifiedPane.setStyle("-fx-background-image: url("+imageUrl+")");
		}else{
			picture.setStyle("-fx-background-color: gray");
			minifiedPane.setStyle("-fx-background-color: gray");
		}
		picture.getStyleClass().add("background");
		minifiedPane.getStyleClass().add("background");
		
		// Panel que mostra se o item do grupo foi selecionado
		isSelectedPane = new BorderPane();
		isSelectedPane.setVisible(false);
		picture.setCenter(isSelectedPane);
		isSelectedPane.setStyle("-fx-background-color: rgba(0,0,0,0.7)");
		GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
		isSelectedPane.setCenter(
				fontAwesome.create(FontAwesome.Glyph.CHECK_CIRCLE).
				color(Color.WHITE).size(60));
		
		// Coloca os componentes no pane
		this.setCenter(picture);
		this.setBottom(titulo);
		
		// Adiciona o item no popOver no botão de adicionar grupo da navbar
		HomeController hc = HomeController.MAIN_INSTANCE;
		groupPopupList = hc.getAddGroupPopUp();
		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				groupPopupList.getItems().add(title);
				
			}
			
		});
			
				
	}
	
	/**
	 * Esse construtor carrega todas as informações
	 * de um grupo e transforma no pane
	 * 
	 * @param grupo Grupo que deverá ser transformado
	 * @param facebookInovar Conta do facebook que usará ogrupo
	 * @throws FacebookException Exceção lançada caso não seja 
	 * possível recolher informações do grupo
	 */
	public GroupItemPaneView(Group grupo, FacebookInovar facebookInovar) 
			throws FacebookException{		
		
		this(facebookInovar.getFacebook().getGroupPictureURL(
				grupo.getId()).toString(), grupo.getName());
		this.grupo = grupo;
		this.facebookInovar = facebookInovar;

		// Listener de quando é adicionado ou removido um item pela 
		// popup list do menu
		groupPopupList.getCheckModel().getCheckedItems().addListener(
				new ListChangeListener<String>() {

			@Override
			public void onChanged(
					javafx.collections.ListChangeListener.Change<
					? extends String> c) {
				while(c.next()){
					
					if(c.wasAdded()){
						if(c.getAddedSubList().contains(title)){
							if(!facebookInovar.getGroups().contains(grupo))
								facebookInovar.getGroups().addFirst(getGrupo());
						}
					}
					
					if(c.wasRemoved()){
						if(c.getRemoved().contains(title)){
							if(facebookInovar.getGroups().contains(grupo))
								facebookInovar.getGroups().remove(getGrupo());
						}
					}
					
				}
			}
			
		});
		
		// Listener de quando é adicionado um grupo novo
		facebookInovar.getGroups().addOnPutItemEvent(new GroupEvent() {
			
			@Override
			public void handle(Group group) {
				if(grupo == group){
					
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							groupPopupList.getCheckModel().check(title);
							
						}
					});
						
					setUsadoParaDivulgacao(true);
					HomeController.MAIN_INSTANCE.tileGroupsPane.getChildren().add(minifiedPane);
				}
			}
		});
		
		// Listener de quando é removido um grupo
		facebookInovar.getGroups().addOnRemoveItemEvent(new GroupEvent() {
			
			@Override
			public void handle(Group group) {
				if(grupo == group){
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							groupPopupList.getCheckModel().clearCheck(title);
							
						}
					});
					setUsadoParaDivulgacao(false);
					HomeController.MAIN_INSTANCE.tileGroupsPane.getChildren().
					remove(minifiedPane);
				}
			}
		});
	}
	
	/**
	 * Função que muda o gráfico do item do facebook para usado
	 * 
	 * @param usado Diz se o grupo esta sendo usado ou não
	 */
	public void setUsadoParaDivulgacao(boolean usado){
		isSelectedPane.setVisible(usado);
	}
	
	/**
	 * Função chamada quando o usuario clica no
	 * grupo na tela
	 * 
	 * @param event informações do evento
	 */
	public void onClickGroupEvent(Event event){
		setUsadoParaDivulgacao(!isSelectedPane.isVisible());
		if(facebookInovar != null){
			if(!facebookInovar.getGroups().contains(getGrupo()))
				facebookInovar.getGroups().addFirst(grupo);
			else
				facebookInovar.getGroups().remove(grupo);
		}
	}
	
	/**
	 * @return valor do isSelectedPane
	 */
	public BorderPane getIsSelectedPane() {
		return isSelectedPane;
	}

	/**
	 * @param isSelectedPane: o isSelectedPane que sera definido
	 */
	public void setIsSelectedPane(BorderPane isSelectedPane) {
		this.isSelectedPane = isSelectedPane;
	}

	/**
	 * @return valor do grupo
	 */
	public Group getGrupo() {
		return grupo;
	}

	/**
	 * @param grupo: o grupo que sera definido
	 */
	public void setGrupo(Group grupo) {
		this.grupo = grupo;
	}

	/**
	 * @return valor do imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl: o imageUrl que sera definido
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return valor do title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title: o title que sera definido
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return valor do facebookInovar
	 */
	public FacebookInovar getFacebookInovar() {
		return facebookInovar;
	}

	/**
	 * @param facebookInovar: o facebookInovar que sera definido
	 */
	public void setFacebookInovar(FacebookInovar facebookInovar) {
		this.facebookInovar = facebookInovar;
	}
	
}
