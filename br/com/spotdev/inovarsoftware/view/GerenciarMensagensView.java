/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import java.io.IOException;

import org.controlsfx.control.PopOver;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import br.com.spotdev.inovarsoftware.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description View da tela gerenciar mensagens
 */
public class GerenciarMensagensView extends View {

	/*
	 * @description Procedimento carregado da View
	 */
	@Override
	public void carregar(Stage mainStage) throws IOException {
		
		Stage stage = new Stage();
		setStage(stage);
		stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(mainStage.getScene().getWindow());
		
		FXMLLoader loader = new FXMLLoader(getClass().
				getResource("fxml/gerenciar-mensagens.fxml"));
		Parent root = loader.load();
		
		if(!(loader.getController() instanceof Controller)){
			System.err.println(
					"Essa view só aceita controllers que sejam instancias de "
					+ "controller.Controller");
			return;
		}
		
	    Controller controller = (Controller)loader.getController();
		controller.setView(this);	
		this.setController(controller);
	    
		int minWidth = 390;
		int minHeight = 490;
        Scene scene = new Scene(root, minWidth, minHeight);
    
        stage.setTitle("InovarSoftware | Gerenciar mensagens");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMinHeight(minHeight);
        
        // Chama o evento depois de ter criado stage
        controller.beforeCreateStage();
		
	}
	
	/**
	 * Carrega o componente de dica que é um popUp
	 * 
	 * @return Retorna o popover com a dica
	 */
	public PopOver getDica(){
		
		BorderPane bp = new BorderPane();
		bp.setPadding(new Insets(30.0));
		
		VBox labels = new VBox();
		Label labelTitle = new Label("Dica!");
		Label labelInfo = new Label("\nVocê pode usar "
				+ "variaveis para melhor\n"
				+ "interação com o usuario nas mensagens:");
		Label labelVar = new Label("\n\n{nome} - Mostrará o "
				+ "nome do usuario;\n"
				+ "{primeiro_nome} - Mostrará o primeiro nome\n"
				+ "{ultimo_nome} - Mostrará o último nome");
		
		labelTitle.setTextFill(Color.GRAY);
		labelTitle.setFont(Font.font(null, FontWeight.BOLD, 20));
		labelVar.setFont(Font.font(null, FontWeight.BOLD, 14));
		
		labels.getChildren().add(labelTitle);
		labels.getChildren().add(labelInfo);
		labels.getChildren().add(labelVar);
		bp.setCenter(labels);
		PopOver vocePodeUsarVariavel = new PopOver(bp);
		return vocePodeUsarVariavel;
		
	}
	
	/**
	 * @author Davi Salles
	 * @description Componente de mensagem para o Accordion
	 * da view
	 */
	public static class MessageItemComponent extends TitledPane {

		/**
		 * Mensagem que o item representa
		 */
		private String message;
		
		/**
		 * Botão usado para remover o item
		 */
		private Button closeButton;
		
		public MessageItemComponent(String message){
		
			this.message = message;
			
			// Título do titledPane
			int messageSize = message.length();
			String title = messageSize > 40 ? 
					message.substring(0, 40)+"..." : message;
			title = title.replaceAll("\n", "");
			setText(title);
			
			// Content do titledPane
			AnchorPane anchorPane = new AnchorPane();
			anchorPane.setPadding(new Insets(10));
			
			// Mensagem do titledPane
			Text messageText = new Text(message);
			messageText.setWrappingWidth(200.0);
			
			// Botão para remover a mensagem
			GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
			closeButton = new Button("",
					fontAwesome.create(FontAwesome.Glyph.TRASH).
					color(Color.WHITE).size(12));
					
			
			// Adiciona tudo ao componente
			anchorPane.getChildren().add(messageText);
			AnchorPane.setLeftAnchor(messageText, 20.0);
			AnchorPane.setTopAnchor(messageText, 20.0);
			
			anchorPane.getChildren().add(closeButton);
			AnchorPane.setRightAnchor(closeButton, 20.0);
			AnchorPane.setBottomAnchor(closeButton, 20.0);
			
			setContent(anchorPane);
			
		}
		
		/**
		 * Define o onClickTrashButton;
		 */
		public void setOnClickTrashButtonEvent(EventHandler<ActionEvent> onClickTrashButton){
			closeButton.setOnAction(onClickTrashButton);
		}

		/**
		 * @return valor do message
		 */
		public String getMessage() {
			return message;
		}

		/**
		 * @param message: o message que sera definido
		 */
		public void setMessage(String message) {
			this.message = message;
		}
		
	}

}
