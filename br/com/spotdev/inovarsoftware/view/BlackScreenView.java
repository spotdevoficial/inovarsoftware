/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeInLeftBigTransition;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeOutUpTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * @author Davi Salles
 * @description View para panes que apareceram no
 * home como uma tela preta meio transparente
 */
public class BlackScreenView extends AnchorPane {

	/**
	 * HomeController a quem o blackScreenPane pertence
	 */
	private HomeController homeController;
	
	/**
	 * Tela preta do fundo da tela
	 */
	private AnchorPane backgroundPane = new AnchorPane();
	
	
	/**
	 * Evento chamado quando fechar o blackScreen
	 */
	private EventHandler<ActionEvent> onCloseEvent;
	
	public BlackScreenView(HomeController homeController){
		this.homeController = homeController;
		backgroundPane = new AnchorPane();
		
		// Gera um painel para o fundo da tela
		AnchorPane.setRightAnchor(backgroundPane, 0.0);
		AnchorPane.setLeftAnchor(backgroundPane, 0.0);
		AnchorPane.setTopAnchor(backgroundPane, 0.0);
		AnchorPane.setBottomAnchor(backgroundPane, 0.0);
		backgroundPane.setStyle("-fx-background-color: rgb(0,0,0,0.9)");
		
		// Cria o botão de fechar o painel
		GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
		Button button = new Button("",fontAwesome.create(FontAwesome.Glyph.CLOSE).color(Color.WHITE).size(60));
		button.setOnAction(this::onClickButtonClose);
		button.setStyle("-fx-background-color: null");
		backgroundPane.getChildren().add(button);
		AnchorPane.setRightAnchor(button, 20.0);
		AnchorPane.setTopAnchor(button, 10.0);
		
		// Adiciona o painel do BlackScreenPane
		backgroundPane.getChildren().add(this);
		AnchorPane.setRightAnchor(this, 60.0);
		AnchorPane.setLeftAnchor(this, 60.0);
		AnchorPane.setTopAnchor(this, 70.0);
		AnchorPane.setBottomAnchor(this, 70.0);
		
		backgroundPane.setVisible(false);
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				getHomeController().mainPane.getChildren().add(backgroundPane);
			}
			
		});
			
	}

	public void mostrar(){
		FadeInLeftBigTransition flbt = new FadeInLeftBigTransition(getBackgroundPane());
		flbt.play();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				getBackgroundPane().setVisible(true);
			}
		}).start();
			
	}
	
	/**
	 * Evento de quanto o usuario clica no botãode fechar o pane 
	 *
	 * @param e informações do evento
	 */
	public void onClickButtonClose(ActionEvent e){
		
		FadeOutUpTransition fade = new FadeOutUpTransition(backgroundPane);
		fade.setDelay(Duration.ZERO);
		fade.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				getBackgroundPane().setVisible(false);
			}
		});
		
		fade.play();
		
		if(onCloseEvent != null)
			onCloseEvent.handle(e);
		
	}

	/**
	 * @return valor do onCloseEvent
	 */
	public EventHandler<ActionEvent> getOnCloseEvent() {
		return onCloseEvent;
	}

	/**
	 * @param onCloseEvent: o onCloseEvent que sera definido
	 */
	public void setOnCloseEvent(EventHandler<ActionEvent> onCloseEvent) {
		this.onCloseEvent = onCloseEvent;
	}

	/**
	 * @return valor do backgroundPane
	 */
	public AnchorPane getBackgroundPane() {
		return backgroundPane;
	}



	/**
	 * @param backgroundPane: o backgroundPane que sera definido
	 */
	public void setBackgroundPane(AnchorPane backgroundPane) {
		this.backgroundPane = backgroundPane;
	}



	/**
	 * @return valor do homeController
	 */
	public HomeController getHomeController() {
		return homeController;
	}

	/**
	 * @param homeController: o homeController que sera definido
	 */
	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}
	
	
	
}
