/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * @author Davi Salles
 * @description Item da navbar direita de mensagem
 */
public class SendMessageItemView extends AnchorPane {

	private String pictureUrl;
	private String time;
	private String message;
	
	public SendMessageItemView(String pictureUrl, String time, String message){
		
		this.pictureUrl = pictureUrl;
		this.time = time;
		this.message = message;
		
		this.setPrefHeight(73);
		this.setPrefWidth(242);
		this.setPadding(new Insets(5,0,5,0));
		getStyleClass().add("anchor-pane");
		
		BorderPane imagePane = new BorderPane();
		imagePane.setPrefHeight(50);
		imagePane.setPrefWidth(50);
		imagePane.setPickOnBounds(true);
		imagePane.getStyleClass().add("background");
		imagePane.setStyle("-fx-background-image: url("+pictureUrl+")");
		AnchorPane.setTopAnchor(imagePane, 4.0);
		AnchorPane.setLeftAnchor(imagePane, 22.0);
		
		Label timeLabel = new Label(time);
		AnchorPane.setLeftAnchor(timeLabel, 30.0);
		AnchorPane.setTopAnchor(timeLabel, 55.0);
		timeLabel.setFont(new Font(12));
		
		Text texto = new Text(message);
		texto.setWrappingWidth(150.0);
		texto.setFill(Color.WHITE);
		texto.setFont(new Font(12));
		AnchorPane.setLeftAnchor(texto, 76.0);
		AnchorPane.setTopAnchor(texto, 16.0);
		
		this.getChildren().add(imagePane);
		this.getChildren().add(timeLabel);
		this.getChildren().add(texto);
	}

	/**
	 * @return valor do pictureUrl
	 */
	public String getPictureUrl() {
		return pictureUrl;
	}

	/**
	 * @param pictureUrl: o pictureUrl que sera definido
	 */
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	/**
	 * @return valor do time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time: o time que sera definido
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return valor do user
	 */
	public String getUser() {
		return message;
	}

	/**
	 * @param user: o user que sera definido
	 */
	public void setUser(String user) {
		this.message = user;
	}
	
	
	
}
