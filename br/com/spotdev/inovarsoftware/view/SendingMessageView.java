/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.controller.SendingMessageController;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description View que contém o carregamento das mensagens
 */
public class SendingMessageView extends View {

	/**
	 * Tela preta do fundo da view
	 */
	private BlackScreenView blackScreen;
	
	/**
	 * HomeController em que a view será carregada
	 */
	private HomeController homeController;
	
	/**
	 * Label que mostra as informações do carregamento
	 */
	private Label labelInfo;

	/**
	 * Barra de carregamento 
	 */
	private ProgressBar progress;

	/**
	 * Foto do usuario que esta tendo sua mensagem enviada
	 */
	private Rectangle userPicture;

	/**
	 * Label com a quantidade de usuarios que faltam
	 */
	private Label labelQuantidade;
	
	/**
	 * @param homeController HomeController em que a view vai ser carregada
	 */
	public SendingMessageView(HomeController homeController) {
		this.homeController = homeController;
	}
	
	/*
	 * @description Cria a view e seus componentes
	 */
	@Override
	public void carregar(Stage stage) throws IOException {
		
		setStage(stage);
		setController(new SendingMessageController());
		getController().setView(this);
		blackScreen = new BlackScreenView(homeController);
		
		// Adiciona o painel que ficara os componentes no centro
		VBox mainPane = new VBox();
		mainPane.setSpacing(10);
		mainPane.setAlignment(Pos.CENTER);
		AnchorPane.setTopAnchor(mainPane, 50.0);
		AnchorPane.setRightAnchor(mainPane, 50.0);
		AnchorPane.setBottomAnchor(mainPane, 50.0);
		AnchorPane.setLeftAnchor(mainPane, 50.0);
		blackScreen.getChildren().add(mainPane);
		
		// Adiciona o avatar do usuario que esta recendo a mensagem
		userPicture = new Rectangle(150, 200);
		userPicture.getStyleClass().add("foto");
		userPicture.getStyleClass().add("background");
		mainPane.getChildren().add(userPicture);
		
		// Informação do carregamento
		labelQuantidade = new Label();
		labelQuantidade.getStyleClass().add("quantidade_mensagens");
		labelQuantidade.setText("0/Calculando...");
		mainPane.getChildren().add(labelQuantidade);
		
		// Informação do carregamento
		labelInfo = new Label();
		labelInfo.getStyleClass().add("informacao_mensagens");
		labelInfo.setText("Carregando informações");
		mainPane.getChildren().add(labelInfo);
		
		// Adiciona a progressbar do carregamento
		progress = new ProgressBar();
		progress.setPrefWidth(500);
		progress.setProgress(0);
		mainPane.getChildren().add(progress);
		
		getController().beforeCreateStage();
		
	}

	/*
	 * @description Mostra a tela do backScreen
	 */
	@Override
	public void mostrar() {
		getBlackScreen().mostrar();
	}
	
	/**
	 * @return valor do labelQuantidade
	 */
	public Label getLabelQuantidade() {
		return labelQuantidade;
	}

	/**
	 * @param labelQuantidade: o labelQuantidade que sera definido
	 */
	public void setLabelQuantidade(Label labelQuantidade) {
		this.labelQuantidade = labelQuantidade;
	}

	/**
	 * @return valor do blackScreen
	 */
	public BlackScreenView getBlackScreen() {
		return blackScreen;
	}

	/**
	 * @param blackScreen: o blackScreen que sera definido
	 */
	public void setBlackScreen(BlackScreenView blackScreen) {
		this.blackScreen = blackScreen;
	}

	/**
	 * @return valor do homeController
	 */
	public HomeController getHomeController() {
		return homeController;
	}

	/**
	 * @param homeController: o homeController que sera definido
	 */
	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}

	/**
	 * @return valor do labelInfo
	 */
	public Label getLabelInfo() {
		return labelInfo;
	}

	/**
	 * @param labelInfo: o labelInfo que sera definido
	 */
	public void setLabelInfo(Label labelInfo) {
		this.labelInfo = labelInfo;
	}

	/**
	 * @return valor do progress
	 */
	public ProgressBar getProgress() {
		return progress;
	}

	/**
	 * @param progress: o progress que sera definido
	 */
	public void setProgress(ProgressBar progress) {
		this.progress = progress;
	}

	/**
	 * @return valor do userPicture
	 */
	public Rectangle getUserPicture() {
		return userPicture;
	}

	/**
	 * @param userPicture: o userPicture que sera definido
	 */
	public void setUserPicture(Rectangle userPicture) {
		this.userPicture = userPicture;
	}
	
	
	
}
