package br.com.spotdev.inovarsoftware.view;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.controller.Controller;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description Interface para a view
 */

public abstract class View {
	
	/**
	 * Controller da classe
	 */
	private Controller controller = null;
	
	/**
	 * Stage da view
	 */
	private Stage stage = null;
	
	/**
	 * @param stage	Stage em que a view esta sendo carregada
	 * @param controller Controller da view
	 * @throws IOException Exceção de caso o arquivo fxml não seja encontrado
	 */
	public abstract void carregar(Stage stage) throws IOException;
	
	
	/**
	 * Mostra a view se já tiver carregada
	 */
	public void mostrar(){
		if(getStage() != null)
			getStage().show();
	}

	/**
	 * @return valor do controller
	 */
	public Controller getController() {
		return controller;
	}


	/**
	 * @param controller: o controller que sera definido
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}
	
	/**
	 * @return valor do stage
	 */
	public Stage getStage() {
		return stage;
	}


	/**
	 * @param stage: o stage que sera definido
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}


	/**
	 * @param stage O Stage em que a view será carregada
	 * @param controller O Controller em que a view sera carregada
	 * @throws IOException Erro caso o FXML não seja encontrado
	 */
	public void carregarEMostrar(Stage stage)  throws IOException {
		carregar(stage);
		mostrar();
	}

	

}
