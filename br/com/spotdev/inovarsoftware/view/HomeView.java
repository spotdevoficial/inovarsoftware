/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import insidefx.undecorator.UndecoratorScene;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.controller.Controller;
import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.controller.SplashPaneController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description View da página inicial
 */
public class HomeView extends View {
	
	@Override
	public void carregar(Stage stage) throws IOException {
				
		SplashPaneController.updateLoad();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/home.fxml"));
		Parent root = loader.load();
		SplashPaneController.updateLoad();
		
		if(!(loader.getController() instanceof Controller)){
			System.err.println("Essa view só aceita controllers que sejam instancias de controller.Controller");
			return;
		}
		
	    Controller controller = (Controller)loader.getController();
		controller.setView(this);
		setController(controller);
		SplashPaneController.updateLoad();
	    
		int minWidth = 950;
		int minHeight = 670;
        UndecoratorScene scene = new UndecoratorScene(stage, (Region) root);
    
        SplashPaneController.updateLoad();
        stage.setTitle("InovarSoftware | Sistema de automação para facebook");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMinHeight(minHeight);
        SplashPaneController.updateLoad();
        setStage(stage);
        
        HomeController.MAIN_INSTANCE = (HomeController) controller;
        SplashPaneController.updateLoad();
        controller.beforeCreateStage();
        SplashPaneController.updateLoad();
	}

	
}
