/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.controller.Controller;
import br.com.spotdev.inovarsoftware.controller.LoginFacebookController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description View da tela de login
 */
public class LoginFacebookView extends View {

	/*
	 * @description Procedimento carregado da View
	 */
	@Override
	public void carregar(Stage mainStage) throws IOException {
		
		Stage stage = new Stage();
		setStage(stage);
		stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(mainStage.getScene().getWindow());
		
		FXMLLoader loader = new FXMLLoader(getClass().
				getResource("fxml/login-facebook.fxml"));
		Parent root = loader.load();
		
		if(!(loader.getController() instanceof Controller)){
			System.err.println("Essa view só aceita controllers "
					+ "que sejam instancias de controller.Controller");
			return;
		}
		
	    Controller controller = (Controller)loader.getController();
		controller.setView(this);	
		this.setController(controller);
	    
		int minWidth = 450;
		int minHeight = 400;
        Scene scene = new Scene(root, minWidth, minHeight);
    
        stage.setTitle("InovarSoftware | Entrar no facebook");
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMinHeight(minHeight);
        stage.setResizable(false);
        
        // Chama o evento depois de ter criado stage
        ((LoginFacebookController)getController()).beforeCreateStage();
		
	}

}
