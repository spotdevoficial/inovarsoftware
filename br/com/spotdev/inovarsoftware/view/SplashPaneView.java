/**
 * 
 */
package br.com.spotdev.inovarsoftware.view;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.controller.Controller;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author Davi Salles
 * @description View da tela do splashpane
 */
public class SplashPaneView extends View {

	/*
	 * @description Procedimento carregado da View
	 */
	@Override
	public void carregar(Stage mainStage) throws IOException {
		
		Stage stage = new Stage();
		setStage(stage);
		
		FXMLLoader loader = new FXMLLoader(getClass().
				getResource("fxml/splashpane.fxml"));
		Parent root = loader.load();
		
		if(!(loader.getController() instanceof Controller)){
			System.err.println("Essa view só aceita controllers "
					+ "que sejam instancias de controller.Controller");
			return;
		}
		
	    Controller controller = (Controller)loader.getController();
		controller.setView(this);	
		this.setController(controller);
	    
		int minWidth = 472;
		int minHeight = 260;
        Scene scene = new Scene(root, minWidth, minHeight);
    
        stage.setTitle("InovarSoftware | Carregando...");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.setMinWidth(minWidth);
        stage.setMinHeight(minHeight);
        stage.setResizable(false);
        
        // Chama o evento depois de ter criado stage
        getController().beforeCreateStage();
		
	}

}
