package br.com.spotdev.inovarsoftware;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.controller.SplashPaneController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description Classe principal do sistema
 */
public class InovarSoftware extends Application {

	/**
	 * Caso verdadeiro, vai desabilidar a ncessidade de login no facebook
	 */
	public static final boolean DISABLE_FACEBOOK = false;
	
	/**
	 * Máximo de membros que o sistema pode enviar mensagens por grupo
	 */
	public static final int LIMIT_MEMBERS = 100000;
	
	/**
	 * Cookie manager que ficará com os cookies do facebook
	 */
	public static CookieManager COOKIE_MANAGER = null;
	
	public static void main(String args[]){
		COOKIE_MANAGER = new CookieManager( null, CookiePolicy.ACCEPT_ALL ) ;
		CookieHandler.setDefault(COOKIE_MANAGER);
		launch(args);
		
	}

	@Override
	public void start(Stage stage) throws Exception {
		
		// Mostra o splash pane
		SplashPaneController.inicar(stage);

		// Cria um delay para poder ter carregado o splashPane
		// TODO Remover o delay quando o DOM estiver pronto
		new Thread(()-> {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				Platform.runLater(()-> {
					SplashPaneController.updateLoad();
					Font.loadFont(
							InovarSoftware.class.getResourceAsStream(
									"/br/com/spotdev/inovarsoftware/view/fonts/"
									+ "fontawesome-webfont-ttf"), 10);
					SplashPaneController.updateLoad();
					HomeController.iniciar(stage);
				});
			
		}).start();
			
		
	}
	
}
