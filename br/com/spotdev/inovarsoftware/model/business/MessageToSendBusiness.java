/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.business;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import br.com.spotdev.inovarsoftware.controller.SendMessagesController;
import br.com.spotdev.inovarsoftware.controller.SendingMessageController;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.model.value.MessageToSend;
import br.com.spotdev.inovarsoftware.utils.MessageUtils;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PictureSize;
import javafx.application.Platform;

/**
 * @author Davi Salles
 * @description Bussines do model SendMessage
 */
public class MessageToSendBusiness {
	
	/**
	 * Função que envia mensagens para um usuario do facebook.
	 *
	 * @param messageToSend Model da mensagem para enviar
	 * @throws InterruptedException Exception de caso não seja possível
	 * add a messageToSend na fila
	 */
	public static void sendMessage(MessageToSend mst) 
			throws InterruptedException{
		
		SendingMessageController smc = 
				SendMessagesController.SENDING_MSG_CONTROLER;
							
		smc.setProgress(0);
		FacebookInovar fi = mst.getAccount();
		Facebook facebook = fi.getFacebook();
		String id = mst.getToId();
		
		try {
			smc.setUserName(facebook.getUser(id).getName());
		} catch (FacebookException e2) {
			e2.printStackTrace();
		}
				
		
		new Thread(()->{
			try {
				String pictureUrl = facebook.getPictureURL(id, 
						PictureSize.large).toString();
				Platform.runLater(()->smc.setPictureUrl(pictureUrl));
			} catch (FacebookException e1) {
				e1.printStackTrace();
			}
		},"Thread de trocar a foto do da tela de carregamento").start();
		
		
		smc.setProgress(0.20);
		
		StringBuilder sb = new StringBuilder(
				"https://www.facebook.com/ajax/mercury/send_messages.php");
		String url = sb.toString();
		
		try {
			
			URL obj = new URL(url);
			HttpsURLConnection con = 
					(HttpsURLConnection) obj.openConnection();

			smc.setProgress(0.4);
			
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", 
					"application/x-www-form-urlencoded");
			con.setRequestProperty("Referer", 
					"https://www.facebook.com/"+id);
			con.setRequestProperty("Host", 
					"www.facebook.com");
			con.setRequestProperty("Origin", 
					"https://www.facebook.com");
			con.setRequestProperty("User-Agent", 
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) "
					+ "AppleWebKit/600.3.18 (KHTML, like Gecko)"
					+ " Version/8.0.3 Safari/600.3.18");
			con.setRequestProperty("Connection", "keep-alive");

			String urlParameters = formatedPostData(mst);
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = 
					new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			smc.setProgress(0.8);
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			
			smc.setProgress(0.9);
			in.close();
			
			smc.setProgress(1);
			
		} catch (IOException | IllegalStateException |
				FacebookException e) {
			e.printStackTrace();
		}
		
		
	}
	
	long last = 0;
	public static String formatedPostData(MessageToSend message) 
			throws IllegalStateException, FacebookException, 
			UnsupportedEncodingException{
		
		SendingMessageController smc = 
				SendMessagesController.SENDING_MSG_CONTROLER;
		FacebookInovar fi = HomeController.MAIN_INSTANCE.getFacebookInovar();
		
		String myId = fi.getThreadId();
		String toUseRId = MessageUtils.getUserId(message.getNome());
		smc.setProgress(0.5);
		
		// Dados para o post
		Date date = new Date();
		String dateFormated = new SimpleDateFormat("HH:mm").format(date);
		long timestamp = date.getTime();
		String time_relative = dateFormated;
		
		// Offline thread id
		long value3 = (long)Math.floor(Math.random() * 4294967295L);
		String floorValue = Long.toString(value3,2);
		String offlineThreadStr = ("0000000000000000000000" + floorValue);
		offlineThreadStr = 
				offlineThreadStr.substring(offlineThreadStr.length()-22);
		String msgsOTI = Long.toString(date.getTime(),2) + offlineThreadStr;
		String offLineThread = MessageUtils.binaryToDecimal(msgsOTI);
		
		smc.setProgress(0.6);
		
		String urlParameters = 
			"message_batch[0][action_type]="+
					enc("ma-type:user-generated-message")+
			"&message_batch[0][thread_id]"+
			"&message_batch[0][author]="+enc("fbid:"+myId)+
			"&message_batch[0][author_email]"+
			"&message_batch[0][timestamp]="+enc(timestamp+"")+
			"&message_batch[0][timestamp_absolute]=Hoje"+
			"&message_batch[0][timestamp_relative]="+enc(""+time_relative)+
			"&message_batch[0][timestamp_time_passed]=0"+
			"&message_batch[0][is_unread]=false"+
			"&message_batch[0][is_forward]=false"+
			"&message_batch[0][is_filtered_content]=false"+
			"&message_batch[0][is_filtered_content_bh]=false"+
			"&message_batch[0][is_filtered_content_account]=false"+
			"&message_batch[0][is_filtered_content_quasar]=false"+
			"&message_batch[0][is_filtered_content_invalid_app]=false"+
			"&message_batch[0][is_spoof_warning]=false"+
			"&message_batch[0][source]="+enc("source:titan:web")+
			"&&message_batch[0][body]="+enc(message.getMessage())+
			"&message_batch[0][has_attachment]=false"+
			"&message_batch[0][html_body]=false"+
			"&&message_batch[0][specific_to_list][0]="+enc("fbid:"+toUseRId)+
			"&message_batch[0][specific_to_list][1]="+enc("fbid:"+myId)+
			"&message_batch[0][force_sms]=true"+
			"&message_batch[0][ui_push_phase]=V3"+
			"&message_batch[0][status]=0"+
			"&message_batch[0][offline_threading_id]="+enc(""+offLineThread)+
			"&message_batch[0][message_id]="+enc(""+offLineThread)+
			"&message_batch[0][manual_retry_cnt]=0"+
			"&client=web_messenger";
		
		String htmlPage = MessageUtils.getMyPageHtml();
		urlParameters = 
				urlParameters+MessageUtils.makeMergeWithDefaults(htmlPage, 
						myId);
		
		smc.setProgress(0.7);
		
		return urlParameters;
		
	}
	
	/**
	 * Encoda uma string para html
	 * 
	 * @param s Código html a ser encodado
	 * @return retorna o código encodado
	 * @throws UnsupportedEncodingException Erro caso não de para encodar
	 */
	private static String enc(String s) throws UnsupportedEncodingException{
		String formated = URLEncoder.encode(s,"UTF-8");
		return formated;
	}
	
}
