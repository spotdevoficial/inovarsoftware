/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.business;

import java.util.Random;

import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import facebook4j.Facebook;
import facebook4j.FacebookFactory;

/**
 * @author Davi Salles
 * @description Model Business da FacebookInovar
 */
public class FacebookInovarBusiness {

	/**
	 * App secret e app id da aplicação
	 */
	private static final String APP_ID = "131947103805459";
	private static final String APP_SECRET = "097b253831a2e389996bf1da734cce24";
	
	/**
	 * Extrai a codeAcess do facebook de uma URL
	 * 
	 * @param url que sera extraida a codeAcess
	 * @return Retorna o codeAcess da contado facebook
	 */
	public static String getCodeAcess(String url){
				
		int i = url.indexOf("code=")+5;
		url = url.substring(i);
		int endI = url.indexOf("&");
		if(endI >= 0)
			url = url.substring(0, endI);

		return url;
		
	}
	
	/**
	 * Função usada para criação da FacebookInovar
	 * 
	 * @return Retorna uma conta de facebook da inovar
	 */
	public static FacebookInovar instanceFacebookAccount(){
		
		Facebook facebook = new FacebookFactory().getInstance();
		facebook.setOAuthAppId(APP_ID, APP_SECRET);
		facebook.setOAuthPermissions("publish_actions,user_groups");
		
		String loginUrl = facebook.getOAuthAuthorizationURL(
				"https://www.facebook.com/connect/login_success.html");
		
		FacebookInovar fi = new FacebookInovar();
		fi.setFacebook(facebook);
		fi.setLoginUrl(loginUrl+"&display=popup");
		
		return fi;
		
	}
	
	/**
	 * Pega mensagens aleatorias no sistema
	 * 
	 * @param fi Conta com lista de mensagens
	 * @return Retorna a mensagem aleatória
	 */
	public static String getRandomMessage(FacebookInovar fi){
		
		Random r = new Random();
		int listSize = fi.getMessages().size();
		return fi.getMessages().get(r.nextInt(listSize)).getMessage();
		
	}
	
}
