/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.business;

import java.util.ArrayList;

import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.view.GroupItemPaneView;
import facebook4j.FacebookException;
import facebook4j.Group;
import facebook4j.ResponseList;
import javafx.scene.layout.BorderPane;

/**
 * @author Davi Salles
 * @description Bussines do groupInovar
 */
public class GroupInovarBusiness {
	
	/**
	 * Função para transformar uma lista de grupos em borderPanes, 
	 * fazendo com que se possa adicionar na tela a lista
	 * 
	 * @param groups Lista de grupos do facebook que quer converter para pane
	 * @return Retorna os grupos do facebook convertidos para borderPanes 
	 */
	public static BorderPane[] convertGroupsToPane(ResponseList<Group> groups, FacebookInovar facebookInovar){
		
		ArrayList<BorderPane> groupList = new ArrayList<>(groups.size());
		
		for(Group g : groups){
			
			try {
				GroupItemPaneView bp = new GroupItemPaneView(g, facebookInovar);
				groupList.add(bp);
			} catch (FacebookException e) {
				e.printStackTrace();
			}
			
		}
		
		return groupList.toArray(new BorderPane[groupList.size()]);
		
	}
	
	

}
