/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.value;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Davi Salles
 * @description
 */

@DatabaseTable
public class MessagesModule {
	
	@DatabaseField(id=true)
	private String message;
	
	/**
	 * 
	 */
	public MessagesModule(String message) {
		this.message = message;
	}
	
	/**
	 * 
	 */
	public MessagesModule() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return valor do message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message: o message que sera definido
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
