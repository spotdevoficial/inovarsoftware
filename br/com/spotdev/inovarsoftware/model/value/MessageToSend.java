/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.value;

/**
 * @author Davi Salles
 * @description Classe que representa a mensagem que esta para ser
 * enviada
 */
public class MessageToSend implements Comparable<MessageToSend>{

	/**
	 * Mensagem que sera enviada
	 */
	private String message;
	
	/**
	 * User do usuario que ira receber a mensagem
	 */
	private String toId;
	
	/**
	 * Conta do facebook que esta logada e enviara a mensagem
	 */
	private FacebookInovar account;
	
	/**
	 * Nome do usuario que receberá a mensagem
	 */
	private String nome;
	
	public MessageToSend(String message, String toId, FacebookInovar account, 
			String nome){
		
		this.message = message;
		this.toId = toId;
		this.account = account;
		this.nome = nome;
		
	}

	
	
	/**
	 * @return valor do nome
	 */
	public String getNome() {
		return nome;
	}



	/**
	 * @param nome: o nome que sera definido
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}



	/**
	 * @return valor do message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message: o message que sera definido
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return valor do to
	 */
	public String getToId() {
		return toId;
	}

	/**
	 * @param to: o to que sera definido
	 */
	public void setToId(String toId) {
		this.toId = toId;
	}

	/**
	 * @return valor do account
	 */
	public FacebookInovar getAccount() {
		return account;
	}

	/**
	 * @param account: o account que sera definido
	 */
	public void setAccount(FacebookInovar account) {
		this.account = account;
	}

	/* Função sobreescrita da classe pai
	 */
	@Override
	public int compareTo(MessageToSend o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
