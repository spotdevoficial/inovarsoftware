/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.value;

import com.j256.ormlite.table.DatabaseTable;

import br.com.spotdev.inovarsoftware.model.dao.MessagesModuleDAO;
import br.com.spotdev.inovarsoftware.utils.GroupList;
import facebook4j.Facebook;
import javafx.collections.ObservableList;

/**
 * @author Davi Salles
 * @description Model Value Obect de uma conta do
 * facebook linkado com a inovar
 */

@DatabaseTable(tableName = "facebookinovar")
public class FacebookInovar {

	/**
	 * O facebook do usuário
	 */
	private Facebook facebook;
	
	/**
	 * Url que será usada para logar no facebook
	 */
	private String loginUrl;
	
	/**
	 * Lista de grupos que serão divulgadas as mensagens
	 */
	private GroupList groups = new GroupList();
	
	/**
	 * Id do usuario usado para enviar mensagem
	 */
	private String threadId;
	
	/**
	 * Lista de mensagens adicionadas pelo usuario
	 * para serem enviadas
	 */
	private ObservableList<MessagesModule> messages = MessagesModuleDAO.readAll();
	
	/**
	 * @return valor do loginUrl
	 */
	public String getLoginUrl() {
		return loginUrl;
	}

	/**
	 * @param loginUrl: o loginUrl que sera definido
	 */
	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	/**
	 * @return valor do facebook
	 */
	public Facebook getFacebook() {
		return facebook;
	}

	/**
	 * @return valor do groups
	 */
	public GroupList getGroups() {
		return groups;
	}
	
	/**
	 * @return valor do messages
	 */
	public ObservableList<MessagesModule> getMessages() {
		return messages;
	}
	
	/**
	 * @return o threadId
	 */
	public String getThreadId() {
		return threadId;
	}

	/**
	 * @param threadId é o threadId a ser definido
	 */
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	/**
	 * @param messages: o messages que sera definido
	 */
	public void setMessages(ObservableList<MessagesModule> messages) {
		this.messages = messages;
	}

	/**
	 * @param facebook: o facebook que sera definido
	 */
	public void setFacebook(Facebook facebook) {
		this.facebook = facebook;
	}
	
}
