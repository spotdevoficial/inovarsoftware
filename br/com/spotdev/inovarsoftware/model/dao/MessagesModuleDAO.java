/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.dao;

import java.io.IOException;
import java.util.Collection;

import org.junit.Test;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import br.com.spotdev.inovarsoftware.model.value.MessagesModule;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Davi Salles
 * @description Dao do model de mensagens
 */
public class MessagesModuleDAO {

	public static final String DATABASE_URL = "jdbc:sqlite:dataFile.db";
	
	@Test
	public void testData(){
		salvar(new MessagesModule("teste"));
	}
	
	public static ObservableList<MessagesModule> readAll(){
		
		try{
	
			Dao<MessagesModule,String> accountDao =  getDao();
			return FXCollections.observableArrayList(accountDao.queryForAll());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	private static Dao<MessagesModule, String> getDao(){
		
		try{
			String databaseUrl = DATABASE_URL;
			ConnectionSource connectionSource =
			     new JdbcConnectionSource(databaseUrl);
	
			Dao<MessagesModule,String> accountDao =
			     DaoManager.createDao(connectionSource, MessagesModule.class);
	
	
			TableUtils.createTableIfNotExists(connectionSource, MessagesModule.class);
			close(accountDao);
			return accountDao;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	private static void close(Dao<?, ?> dao) throws IOException{
		dao.getConnectionSource().close();
	}
	
	public static void salvarTodas(Collection<MessagesModule> messages){
		
		try{
		
			Dao<MessagesModule,String> accountDao =
			    getDao();
			for(MessagesModule mm : messages){
				accountDao.createIfNotExists(mm);
			}
			
			close(accountDao);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void remove(MessagesModule message){
		try{
			Dao<MessagesModule,String> accountDao =
				    getDao();
			accountDao.delete(message);
			
			close(accountDao);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void salvar(MessagesModule message){
		
		try{
			Dao<MessagesModule,String> accountDao = getDao();
			accountDao.createIfNotExists(message);
			close(accountDao);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}
