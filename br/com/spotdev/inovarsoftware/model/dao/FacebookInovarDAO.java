/**
 * 
 */
package br.com.spotdev.inovarsoftware.model.dao;

import org.junit.Test;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.model.value.MessagesModule;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Davi Salles
 * @description Dao da conta do facebook
 */
public class FacebookInovarDAO {
	
	@Test
	public void salvar(){
		
		try{
			// this uses h2 but you can change it to match your database
			String databaseUrl = "jdbc:sqlite:facebookinovar.db";
			// create a connection source to our database
			ConnectionSource connectionSource =
			     new JdbcConnectionSource(databaseUrl);
	
			// instantiate the DAO to handle Account with String id
			Dao<FacebookInovar,String> accountDao =
			     DaoManager.createDao(connectionSource, FacebookInovar.class);
	
			// if you need to create the 'accounts' table make this call
			TableUtils.createTable(connectionSource, FacebookInovar.class);
	
			// create an instance of Account
			FacebookInovar fi = new FacebookInovar();
			ObservableList<MessagesModule> ob = FXCollections.observableArrayList();
			ob.add(new MessagesModule("teste"));
			fi.setMessages(ob);
	
			// persist the account object to the database
			accountDao.create(fi);
	
	
			// close the connection source
			connectionSource.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
}
