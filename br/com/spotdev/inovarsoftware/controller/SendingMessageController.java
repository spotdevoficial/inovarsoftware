/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.io.IOException;

import br.com.spotdev.inovarsoftware.view.SendingMessageView;
import javafx.application.Platform;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description Controller para a tela de carregamento do envio de mensagens
 */
public class SendingMessageController extends Controller {

	private String pictureUrl;
	private int maxUsers;
	private String userName;
	private int messagesSent;
	
	public static SendingMessageController iniciar(HomeController controller){
		
		Stage stage = controller.getView().getStage();
		SendingMessageView smv = new SendingMessageView(controller);
		try {
			smv.carregarEMostrar(stage);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		SendingMessageController smc = 
				(SendingMessageController) smv.getController();
		smc.messagesSent = 0;
		return smc;
		
	}
	
	/**
	 * Adiciona um usuario ao contador
	 */
	public void addUserAoContador(){
		
		messagesSent++;
		String contador = messagesSent+"/"+maxUsers;
		setLabelQuantidade(contador);
		System.out.println(contador);
		
	}
	
	public void setLabelQuantidade(String str){
		Platform.runLater(()->((SendingMessageView)getView()).
				getLabelQuantidade().setText(str));
	}
	
	/**
	 * Aumenta o progresso da barrinha de carregamneto
	 */
	public void setProgress(double progress){
		
		Platform.runLater(()->{
			SendingMessageView sv = (SendingMessageView)getView();
			ProgressBar carregamento = sv.getProgress();
			carregamento.setProgress(progress);
		});
		
	}
	
	/**
	 * @return valor do pictureUrl
	 */
	public String getPictureUrl() {
		return pictureUrl;
	}



	/**
	 * @param pictureUrl: o pictureUrl que sera definido
	 */
	public void setPictureUrl(String pictureUrl) {
		SendingMessageView sv = (SendingMessageView)getView();
		Image image = new Image(pictureUrl);
		sv.getUserPicture().setFill(new ImagePattern(image,
				0,0,150,200, false));
		this.pictureUrl = pictureUrl;
	}

	/**
	 * @return valor do maxUsers
	 */
	public int getMaxUsers() {
		return maxUsers;
	}

	/**
	 * @param maxUsers: o maxUsers que sera definido
	 */
	public void setMaxUsers(int maxUsers) {
		this.maxUsers = maxUsers;
		SendingMessageView sv = (SendingMessageView)getView();
		StringBuilder sb = new StringBuilder(messagesSent+"/");
		Platform.runLater(()->sv.getLabelQuantidade().setText(
				sb.append(maxUsers).toString()));
			
	}



	/**
	 * @return valor do userName
	 */
	public String getUserName() {
		return userName;
	}



	/**
	 * @param userName: o userName que sera definido
	 */
	public void setUserName(String userName) {
		this.userName = userName;
		StringBuilder sb = new StringBuilder("Enviando mensagem para ");
		setInfoMessage(sb.append(userName).toString());
	}

	/**
	 * Define a infor aparecendo na tela de carregamento
	 * 
	 * @param message Mensagem que sera definida
	 */
	public void setInfoMessage(String message){
		SendingMessageView sv = (SendingMessageView)getView();
		Platform.runLater(()->sv.getLabelInfo().setText(message));
	}

	/*
	 * @description Função chamada depois da view ser criada
	 */
	@Override
	public void beforeCreateStage() {
		
		
		
	}
	
}
