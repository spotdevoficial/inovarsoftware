/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import br.com.spotdev.inovarsoftware.model.business.GroupInovarBusiness;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.view.BlackScreenView;
import br.com.spotdev.inovarsoftware.view.GroupItemPaneView;
import facebook4j.Group;
import facebook4j.ResponseList;
import javafx.application.Platform;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;

/**
 * @author Davi Salles
 * @description Controller do painel que abre ao clicar 
 * no botão de "mais grupos"
 */
public class FacebookGroupListController {
	
	/**
	 * HomeController a quem o FacebookGroupListController pertence
	 */
	private HomeController homeController;
	
	/**
	 * Tela preta do fundo do facebookGroupList
	 */
	private BlackScreenView blackScreenView;
	
	public FacebookGroupListController(HomeController homeController){
		this.homeController = homeController;
	}
	
	/**
	 * Carrega todos os grupos na tela nos componentes da 
	 * lista de grupos na tela
	 * 
	 * @param groups lista de grupos
	 * @param facebookInovar conta do facebook que contem os grupos
	 */
	public void loadGrupos(ResponseList<Group> groups, FacebookInovar facebookInovar){
		
		blackScreenView = new BlackScreenView(getHomeController());
		ScrollPane scl = new ScrollPane();
		scl.getStyleClass().add("invisible-scroll-pane");
		scl.setHbarPolicy(ScrollBarPolicy.NEVER);
		scl.setFitToWidth(true);
		AnchorPane.setRightAnchor(scl, 0.0);
		AnchorPane.setTopAnchor(scl, 0.0);
		AnchorPane.setLeftAnchor(scl, 0.0);
		AnchorPane.setBottomAnchor(scl, 0.0);
		
		TilePane tp = new TilePane();
		AnchorPane.setRightAnchor(tp, 0.0);
		AnchorPane.setTopAnchor(tp, 0.0);
		AnchorPane.setLeftAnchor(tp, 0.0);
		AnchorPane.setBottomAnchor(tp, 0.0);
		tp.setStyle("-fx-background-color: transparent");
		
		tp.setHgap(50.0);
		tp.setVgap(20.0);
		
		tp.setPrefTileHeight(180);
		tp.setPrefTileWidth(120);
		BorderPane[] borderPanes = GroupInovarBusiness.convertGroupsToPane(groups, facebookInovar);
		for(BorderPane bp : borderPanes){
			tp.getChildren().add(bp);
		}
		
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				scl.setContent(tp);
				blackScreenView.getChildren().add(scl);
			}
		});
		
	}
	
	/**
	 * Caso não va carregar grupo algum, essa função simplesmente 
	 * mostra o itens aleatorios adicionados pelo sistema
	 * 
	 * PS. É necessário que InovarSoftwre.DISABLE_FACEBOOK seja TRUE
	 * para essa função ser chamada no sistema
	 */
	public void show(){
		
		blackScreenView = new BlackScreenView(getHomeController());
	
		ScrollPane scl = new ScrollPane();
		scl.getStyleClass().add("invisible-scroll-pane");
		scl.setHbarPolicy(ScrollBarPolicy.NEVER);
		scl.setFitToWidth(true);
		AnchorPane.setRightAnchor(scl, 0.0);
		AnchorPane.setTopAnchor(scl, 0.0);
		AnchorPane.setLeftAnchor(scl, 0.0);
		AnchorPane.setBottomAnchor(scl, 0.0);
		
		TilePane tp = new TilePane();
		AnchorPane.setRightAnchor(tp, 0.0);
		AnchorPane.setTopAnchor(tp, 0.0);
		AnchorPane.setLeftAnchor(tp, 0.0);
		AnchorPane.setBottomAnchor(tp, 0.0);
		tp.setStyle("-fx-background-color: transparent");

		tp.setHgap(50.0);
		tp.setVgap(20.0);
		
		tp.setPrefTileHeight(180);
		tp.setPrefTileWidth(120);
		
		for(int i = 0; i < 15; i ++){
			GroupItemPaneView morePane = new GroupItemPaneView(null, "Grupo de programação");
			tp.getChildren().add(morePane);
		}
		
		scl.setContent(tp);
		blackScreenView.getChildren().add(scl);
		blackScreenView.mostrar();
		
	}
	
	/**
	 * Mostra os grupos carregados da conta no facebook
	 */
	public void showLoaded(){
		blackScreenView.mostrar();
	}

	/**
	 * @return valor do blackScreenView
	 */
	public BlackScreenView getBlackScreenView() {
		return blackScreenView;
	}

	/**
	 * @param blackScreenView: o blackScreenView que sera definido
	 */
	public void setBlackScreenView(BlackScreenView blackScreenView) {
		this.blackScreenView = blackScreenView;
	}

	/**
	 * @return valor do homeController
	 */
	public HomeController getHomeController() {
		return homeController;
	}

	/**
	 * @param homeController: o homeController que sera definido
	 */
	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}
	
	
	
}
