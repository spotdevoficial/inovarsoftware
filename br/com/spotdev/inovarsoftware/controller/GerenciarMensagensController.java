/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.io.IOException;
import java.util.List;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.model.dao.MessagesModuleDAO;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.model.value.MessagesModule;
import br.com.spotdev.inovarsoftware.view.GerenciarMensagensView;
import br.com.spotdev.inovarsoftware.view.GerenciarMensagensView.MessageItemComponent;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ListChangeListener.Change;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description Controller da tela de gerenciar mensagens
 */
public class GerenciarMensagensController extends Controller {

	/**
	 * Função para iniciar a tela de gerenciar mensagens
	 * 
	 * @param stage Stage em que o a tela sera criada
	 */
	public static void iniciar(Stage stage) {

		GerenciarMensagensView gerenciarMensagensView = 
				new GerenciarMensagensView();
		try {
			gerenciarMensagensView.carregar(stage);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Evento chamado quando o usuario clica no botão de 
	 * adicionar uma nova mensagem
	 */
	@FXML
	public void onClickAddMessage(){
		
		String message = messageTextArea.getText();
		if(!InovarSoftware.DISABLE_FACEBOOK){
			FacebookInovar facebookInovar = 
					HomeController.MAIN_INSTANCE.getFacebookInovar();
			facebookInovar.getMessages().add(new MessagesModule(message));
		}else{
			addNewMessagePane(message);
		}
	}
	
	/**
	 * Evento chamado quando um texto é inserido na TextArea
	 */
	@FXML
	public void onInsertText(){
		
		// Libera o botão adicionar caso a quantidade de texto
		// seja maior que 0
		int lenght = messageTextArea.getText().length();
		if(lenght > 0){
			addMessageButton.setDisable(false);
		}else{
			addMessageButton.setDisable(true);
		}
		
	}
	
	/**
	 * Adiciona uma nova mensagem no accordion da janela
	 * 
	 * @param message Mensagem que sera adicionada
	 */
	private MessageItemComponent addNewMessagePane(String message){
		final MessageItemComponent mic = new MessageItemComponent(message);
		messagesAccordion.getPanes().add(mic);
		
		// Evento de quando é clicado no botão de remover a mensagem
		mic.setOnClickTrashButtonEvent(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if(!InovarSoftware.DISABLE_FACEBOOK)
					HomeController.MAIN_INSTANCE.getFacebookInovar().
					getMessages().remove(mic.getMessage());
				else
					removeMessagePane(mic);
			}
		});
				
		semMensagensLabel.setVisible(false);

		return mic;
	}
	
	/**
	 * Remove um messageItem do accordionPane
	 * 
	 * @param mic Item que sera removido
	 */
	public void removeMessagePane(MessageItemComponent mic){
		messagesAccordion.getPanes().remove(mic);
	}
	
	/**
	 * Adiciona todas as mensagens no accordion
	 */
	public void refreshMessages(){
		
		if(!InovarSoftware.DISABLE_FACEBOOK){
			FacebookInovar fi = HomeController.MAIN_INSTANCE.getFacebookInovar();
			for(MessagesModule msgModule : fi.getMessages()){
				
				String msg = msgModule.getMessage();
				MessageItemComponent mic = addNewMessagePane(msg);
				FacebookInovar facebookInovar = 
						HomeController.
						MAIN_INSTANCE.getFacebookInovar();
				facebookInovar.getMessages().addListener(
						new ListChangeListener<MessagesModule>() {

					// Listener de quando o item é 
					// removido da lista
					@Override
					public void onChanged(javafx.collections.ListChangeListener.
							Change<? extends MessagesModule> c) {
						
						while(c.next()){
							if(c.wasRemoved()){
								
								List<? extends MessagesModule> 
								adicionados = 
								c.getRemoved();
								
								for(MessagesModule mensagem 
										: adicionados){
									if(mensagem.getMessage().
											equalsIgnoreCase
											(
											mic.getMessage()
											)){
										removeMessagePane
										(mic);
									}
								}
							}
						}
					}
				});
				
			}
		}
		
	}
	
	@Override
	public void beforeCreateStage() {
		
		getView().mostrar();
		addMessageButton.setDisable(true);
		refreshMessages();
		
		// Mostra um popOver contando sobre a possibilidade de 
		// usar variaveis no texto
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						GerenciarMensagensView gmv = 
								(GerenciarMensagensView)getView();
						gmv.getDica().show(messageTextArea);						
					}
				});
				
			}
		}).start();
		
		// Evento chamado quando a mensagem é adicionada no sistema pela lista
		if(!InovarSoftware.DISABLE_FACEBOOK){
			
			FacebookInovar facebookInovar = 
					HomeController.MAIN_INSTANCE.getFacebookInovar();
			facebookInovar.getMessages().addListener(this::onAddItemToList);
		}
			
	}
	
	// Evento chamado quando a mensagem é adicionada no sistema pela lista
	private void onAddItemToList(ListChangeListener.Change<? extends MessagesModule> c){
		
		FacebookInovar facebookInovar = 
				HomeController.MAIN_INSTANCE.getFacebookInovar();
		
		while(c.next()){
			if(c.wasAdded()){
				
				List<? extends MessagesModule> adicionados = 
						c.getAddedSubList();
				
				for(MessagesModule mensagemModule : adicionados){
					String mensagem = mensagemModule.getMessage();
					MessageItemComponent mic = 
							addNewMessagePane(mensagem);
					MessagesModuleDAO.salvar(mensagemModule);
					
					if(!InovarSoftware.DISABLE_FACEBOOK){
						
						// Listener de quando o item é 
						// removido da lista
						facebookInovar.getMessages().addListener((ListChangeListener.Change<? extends MessagesModule> c2)-> {
						
							onItemRemovedFromList(c2, mic);
							
						});
					}
				}
			}
		}
		
	}
	
	/**
	 * Listener de qunado o item é removido da lista
	 */
	private void onItemRemovedFromList(Change<? extends MessagesModule> c2, 
			MessageItemComponent mic){
		
		while(c2.next()){
			if(c2.wasRemoved()){
				
				List<? extends MessagesModule> 
				adicionadosR = c2.getRemoved();
				
				for(MessagesModule mensagemR : adicionadosR){
					if(mensagemR.getMessage().equalsIgnoreCase(
							mic.getMessage())){
						MessagesModuleDAO.remove(mensagemR);
						removeMessagePane(mic);
					}
				}
			}
		}
		
	}
	
	/**
	 * Label apresentadas quando não tem mensagens no accordion
	 */
	@FXML Label semMensagensLabel;
	
	/**
	 * Botão para adicionar a mensagem
	 */
	@FXML Button addMessageButton;
	
	/**
	 * TextArea para inserir a nova mensagem
	 */
	@FXML TextArea messageTextArea;
	
	/**
	 * Accordion com mensagens ja adicionadas
	 */
	@FXML Accordion messagesAccordion;

}
