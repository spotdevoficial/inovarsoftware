package br.com.spotdev.inovarsoftware.controller;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.URI;
import java.net.URISyntaxException;

import org.controlsfx.control.CheckListView;
import org.controlsfx.control.PopOver;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.utils.DesktopApi;
import br.com.spotdev.inovarsoftware.view.HomeView;
import br.com.spotdev.inovarsoftware.view.utils.animations.BounceOutDownTransition;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeInTransition;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeOutTransition;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.PictureSize;
import facebook4j.Reading;
import facebook4j.User;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Davi Salles
 * @description Controller do home 
 */
public class HomeController extends Controller {
	
	/**
	 * Controller principal do home
	 */
	public static HomeController MAIN_INSTANCE;
	
	/**
	 * Conta do facebook usada no controller
	 */
	private FacebookInovar facebookInovar;
	
	/**
	 * Controller da areade grupos
	 */
	private GroupAreaController groupArea;
	
	/**
	 * Controller da navbar de mensagens ja enviadas
	 */
	private SendMessagesController messagesController;
	
	/**
	 * Lista do popoup para adicionar grupos do facebook
	 */
	private CheckListView<String> addGroupPopUp = new CheckListView<>();
	
	/**
	 * Inicia o controller
	 */
	public static HomeController iniciar(Stage stage){
		
		HomeView hv = new HomeView();
		
		try {
			hv.carregar(stage);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return (HomeController) hv.getController();
		
	}

	/**
	 * Action listener do botão sair do painel esquerdo
	 */
	@FXML
	public void onButtonSairEvent(ActionEvent event){
		
		final BounceOutDownTransition b = new BounceOutDownTransition(mainPane);
		b.play();
		b.setOnFinished(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});
		
	}
	
	/**
	 * Action listener do botão de entrar no facebook quando ainda não entrou
	 */
	@FXML
	public void onClickEntrarFacebook(ActionEvent event){
		
		buttonEntrarFacebook.setDisable(true);
		LoginFacebookController.iniciar(getView().getStage());
		
	}
	
	/**
	 * @param permitir libera a tela 
	 * @param noeffect verifica se tera ou não tera efeito
	 * 
	 * Permite que o usuario interaja com a tela após logar
	 */
	public void permitirInteracao(boolean permitir, boolean noeffect){
	
		if(!noeffect){
			if(permitir == true){
				FadeOutTransition bot = new FadeOutTransition(opsPane); 
				bot.setDelay(new Duration(1000));
				bot.setOnFinished(new EventHandler<ActionEvent>() {
	
					@Override
					public void handle(ActionEvent event) {
						permitirInteracao(true);
					}
					
				});
				bot.play();
			}else{
				FadeInTransition bot = new FadeInTransition(opsPane);
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						permitirInteracao(false);
					}
				}).start();
				bot.play();
			}
		}else{
			permitirInteracao(permitir);
		}
		
	}
	
	/**
	 * Esconde o painel preto para login no facebook
	 * e permite a interação com o homeController
	 * 
	 * @param permitir Se sera permitido ou não
	 */
	public void permitirInteracao(boolean permitir){
		
		opsPane.setVisible(!permitir);
		if(permitir == true)
			generalPane.setEffect(null);
		else{
			BoxBlur bb = new BoxBlur();
			bb.setHeight(11);
			bb.setWidth(11);
			bb.setIterations(2);
			generalPane.setEffect(bb);
		}
		
	}
	
	/**
	 * Prepara a view para a conta do facebook
	 * 
	 * @param faceInovar: Conta do facebook que será usada
	 */
	public void loadFacebookAccount(FacebookInovar faceInovar){
		
		loadingGroups.setVisible(true);
		this.facebookInovar = faceInovar;
		Facebook facebook = faceInovar.getFacebook();
		try {
			
			// Carrega as informações de perfil
			String profileUrl = facebook.getPictureURL(PictureSize.large).
					toString();
			User user = facebook.getUser(facebook.getId(), new Reading().
					fields("cover").fields("picture"));
			profilePicturePane.setStyle("-fx-background-image: url("+
					profileUrl+")");
			capaPane.setStyle("-fx-background-image: url("+
					user.getCover().getSource()+")");
			nameLabel.setText(facebook.getMe().getName());
			
			// Carrega alguns componentes
			HomeController homeController = this;
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					groupArea = new GroupAreaController(homeController);
					messagesController = 
							new SendMessagesController(homeController);
					
					try {
						groupArea.loadGroupArea(faceInovar); 
					} catch (FacebookException e) {
						e.printStackTrace();
					}
				}
			}).start();
				
			
		} catch (FacebookException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void beforeCreateStage() {
		
		if(InovarSoftware.DISABLE_FACEBOOK){
        	this.permitirInteracao(true);
        	groupArea = new GroupAreaController(this);
        	messagesController = new SendMessagesController(this);
			try {
				groupArea.loadGroupArea(null);
			} catch (FacebookException e) {
				e.printStackTrace();
			}
		}
		
		getView().mostrar();
		SplashPaneController.MAIN_INSTANCE.getView().getStage().close();
		
	}
	
	/**
	 * Evento chamado quando clicado no botão Acessar Site
	 */
	@FXML
	public void onClickButtonAcessarSite(){
		try {
			DesktopApi.browse(new URI("http://programainovar.com.br"));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Evento de quando o usuario clica no botão de trocar conta do
	 * facebook
	 * 
	 * TODO Ajeitar otimização disso para poder testar
	 */
	@FXML 
	public void onClickButtonMudarContaEvent(){
		
		CookieHandler.setDefault(new java.net.CookieManager());
		permitirInteracao(false, false);
		setFacebookInovar(null);
				
	}
	
	/**
	 * Evento de quando o usuario clica em resetar conta do 
	 * software
	 */
	@FXML 
	public void onClickButtonResetarContaEvent(){
	
		facebookInovar.getGroups().clear();
		facebookInovar.getMessages().clear();
				
	}
	
	/**
	 * Evento chamado quando clicado no botão 'Acessar' do accordion Group
	 */
	@FXML 
	public void onClickButtonAdicionarGrupo(){
		
		PopOver po = new PopOver();
		po.setContentNode(getAddGroupPopUp());
		po.show(buttonAdicionarGrupo);
		
	}
	
	/**
	 * Evento chamado quando o usuario pressiona o botão Listar da tab
	 * grupos do accordion
	 */
	@FXML
	public void onClickButtonListarEvent(){
		
		getGroupArea().getFacebookList().showLoaded();
		
	}
	
	/**
	 * Evento de quando o usuario clica em gerenciar mensagens
	 */
	@FXML
	public void onClickGerenciarMensagensButton(){
		GerenciarMensagensController.iniciar(getView().getStage());
	}
	
	/**
	 * @return valor do messagesController
	 */
	public SendMessagesController getMessagesController() {
		return messagesController;
	}

	/**
	 * @param messagesController: o messagesController que sera definido
	 */
	public void setMessagesController(SendMessagesController messagesController) {
		this.messagesController = messagesController;
	}

	/**
	 * @return valor do groupArea
	 */
	public GroupAreaController getGroupArea() {
		return groupArea;
	}

	/**
	 * @param groupArea: o groupArea que sera definido
	 */
	public void setGroupArea(GroupAreaController groupArea) {
		this.groupArea = groupArea;
	}

	/**
	 * @return valor do addGroupPopUp
	 */
	public CheckListView<String> getAddGroupPopUp() {
		return addGroupPopUp;
	}

	/**
	 * @param addGroupPopUp: o addGroupPopUp que sera definido
	 */
	public void setAddGroupPopUp(CheckListView<String> addGroupPopUp) {
		this.addGroupPopUp = addGroupPopUp;
	}

	/**
	 * @return valor do facebookInovar
	 */
	public FacebookInovar getFacebookInovar() {
		return facebookInovar;
	}

	/**
	 * @param facebookInovar: o facebookInovar que sera definido
	 */
	public void setFacebookInovar(FacebookInovar facebookInovar) {
		this.facebookInovar = facebookInovar;
	}



	/**
	 * Botão de sair do final do menu esquerdo
	 */
	@FXML public Button buttonSairLeft;
	
	/**
	 * AnchorPane pai de todo conteudo da home.fxml
	 */
	@FXML public AnchorPane mainPane;
	
	/**
	 * Botão usado para entrar no facebook
	 */
	@FXML public Button buttonEntrarFacebook;
	
	/**
	 * Paido conteúdo da telinha principal, filho do mainPane
	 */
	@FXML public AnchorPane generalPane;
	
	/**
	 * Pane de aviso "Ops você precisa logar no facebook"
	 */
	@FXML public AnchorPane opsPane;
	
	/**
	 * Logo do software canto superior esquerdo
	 */
	@FXML public Label inovarLogo;
	
	/**
	 * Pane que fica a profilePicture
	 */
	@FXML public BorderPane profilePicturePane;
	
	/**
	 * Pane onde fica a capa do usuario
	 */
	@FXML public AnchorPane capaPane;
	
	/**
	 * Label que contem o nome do usuario do face
	 */
	@FXML public Label nameLabel;
	
	/**
	 * TilePane com todos os grupos do usuario
	 */
	@FXML public TilePane tileGroupsPane;

	/**
	 * Ícone de carregamento dos grupos do facebook
	 */
	@FXML public ImageView loadingGroups;
	
	/**
	 * Botão de gerenciar mensagens
	 */
	@FXML public Button gerenciarMensagensButton;
	
	/**
	 * Botão de enviar mensagem
	 */
	@FXML public Button buttonEnviar;
	
	/**
	 * Painel das mensagens ja enviadas para os
	 * usuarios
	 */
	@FXML public VBox paneForMessages;
	
	/**
	 * Botão de adicionar grupo ou remover
	 */
	@FXML public Button buttonAdicionarGrupo;
	
	/**
	 * Botão de listar grupos na blackscreen
	 */
	@FXML public Button buttonListar;
	
	/**
	 * Botão de limpar mensagens no canto superior direito da tela
	 */
	@FXML public Button buttonLimparMensagens;
	
}
