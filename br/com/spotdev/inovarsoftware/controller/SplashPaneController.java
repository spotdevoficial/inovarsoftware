/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.utils.OSValidator;
import br.com.spotdev.inovarsoftware.view.SplashPaneView;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 * @author Davi Salles
 * @description Controller da tela do splashpane
 */
public class SplashPaneController extends Controller {

	/**
	 * Instancia principal do splashPane
	 */
	public static SplashPaneController MAIN_INSTANCE = null;
	
	/**
	 * Quantidade de vezes que a barrinha aumenta
	 */
	public static int NIVEIS_DE_CARREGAMENTO = 10;
	
	/**
	 * Mostra a view do controller 
	 * 
	 * @param stage stage em que a tela ficará
	 */
	public static void inicar(Stage stage){
		
		SplashPaneView spv = new SplashPaneView();
		try {
			spv.carregarEMostrar(stage);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		MAIN_INSTANCE = (SplashPaneController) spv.getController();
		
	}
	
	@Override
	public void beforeCreateStage() {
		
		String mediaUrl = null;
		Node mv = null;
		
		if(!OSValidator.isUnix()){
			try {
				mediaUrl = InovarSoftware.class.getResource(
						"view/video/splash.mp4").toURI().toString();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			Media media = new Media(mediaUrl);
			MediaPlayer mp = new MediaPlayer(media);
			mp.setAutoPlay(true);
			mv = new MediaView(mp);
		}else{
			mv = new Pane();
			mv.getStyleClass().add("splash-background");
		}
		
		mv.setScaleX(2.0);
		mv.setScaleY(2.0);
		AnchorPane.setLeftAnchor(mv, 0.0);
		AnchorPane.setTopAnchor(mv, 0.0);
		AnchorPane.setBottomAnchor(mv, 0.0);
		AnchorPane.setRightAnchor(mv, 0.0);
		paneBackground.setCenter(mv);
		
		
		
	}
	
	/**
	 * Aumenta um nivel na barra de carregamento
	 */
	public static void updateLoad(){
				
		ProgressBar carregamento = 
				SplashPaneController.MAIN_INSTANCE.carregamento;
		
		double atual = carregamento.getProgress();
		double novo = atual+1.0/((double)NIVEIS_DE_CARREGAMENTO);
		carregamento.setProgress(novo);
		
	}
	
	/**
	 * Barra de carregamento do splashpane
	 */
	@FXML public ProgressBar carregamento;

	/**
	 * Painel principal da tela
	 */
	@FXML public AnchorPane mainPane; 
	
	/**
	 * Pane que deverá ser adicionado o background
	 */
	@FXML public BorderPane paneBackground;
	
}
