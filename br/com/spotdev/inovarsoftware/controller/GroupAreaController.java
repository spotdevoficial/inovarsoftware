/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.Group;
import facebook4j.Reading;
import facebook4j.ResponseList;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * @author Davi Salles
 * @description Classe que representa as áreas com o grupo
 * de usuários
 */
public class GroupAreaController  {

	/**
	 * HomeController que contem os controller do grupo
	 */
	private HomeController homeController;
	
	/**
	 * Pane que representa o botão de + grupos
	 */
	private BorderPane morePane;
	
	/**
	 * Conta do facebook que participa dos grupos do controller
	 */
	private FacebookInovar facebookInovar;
	
	/**
	 * Tela de lista de grupos preta
	 */
	private FacebookGroupListController facebookList;
	
	public GroupAreaController(HomeController homeController){
		this.homeController = homeController;
	}

	/**
	 * Carrega o GroupArea na tela
	 * 
	 * @param facebookInovar Conta do facebook que sera carregada nos grupos
	 * @throws FacebookException Exceção lançada caso de erro ao puxar os grupos
	 * da conta do facebook
	 */

	public void loadGroupArea(FacebookInovar facebookInovar) 
			throws FacebookException{
		
		this.facebookInovar = facebookInovar;
		facebookList = new FacebookGroupListController(getHomeController());
		
		// Carrega os grupos que o usuario tem
		HomeController hc = getHomeController();
		if(!InovarSoftware.DISABLE_FACEBOOK){
			Facebook facebook = facebookInovar.getFacebook();
			ResponseList<Group> groups = facebook.getGroups(
					new Reading().limit(200));
			facebookList.loadGrupos(groups, facebookInovar);
		}
		
		// Adiciona o botão para mais grupos
		Label label = new Label("+");
		label.setTextAlignment(TextAlignment.CENTER);
		label.setFont(new Font(72));
		morePane = new BorderPane();
		morePane.getStyleClass().add("more");
		morePane.setCenter(label);
		morePane.setCursor(Cursor.HAND);
		morePane.setOnMousePressed(this::OnClickButtonMore);
		
		Platform.runLater(new Runnable() {
		     @Override public void run() {
		    	 TilePane tp = hc.tileGroupsPane;
		    	 tp.getChildren().add(morePane);
		    	 
		    	 // Remove o ícone de carregamento dos grupos
		    	 hc.loadingGroups.setVisible(false);
		    	 hc.mainPane.getChildren().remove(hc.loadingGroups);
		    	 hc.loadingGroups = null;
		    	 hc.buttonListar.setDisable(false);
		     }
		});
		
		
	}
	
	/**
	 * Função chamada quando o botão de more groups é clicaso
	 * 
	 * @param e especificações do evento
	 */
	public void OnClickButtonMore(Event e){
	
		if(InovarSoftware.DISABLE_FACEBOOK)
			facebookList.show();
		else
			facebookList.showLoaded();
		
	}
	
	/**
	 * @return valor do facebookList
	 */
	public FacebookGroupListController getFacebookList() {
		return facebookList;
	}

	/**
	 * @param facebookList: o facebookList que sera definido
	 */
	public void setFacebookList(FacebookGroupListController facebookList) {
		this.facebookList = facebookList;
	}

	/**
	 * @return valor do homeController
	 */
	public HomeController getHomeController() {
		return homeController;
	}

	/**
	 * @param homeController: o homeController que sera definido
	 */
	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}

	/**
	 * @return valor do morePane
	 */
	public BorderPane getMorePane() {
		return morePane;
	}

	/**
	 * @param morePane: o morePane que sera definido
	 */
	public void setMorePane(BorderPane morePane) {
		this.morePane = morePane;
	}

	/**
	 * @return valor do facebookInovar
	 */
	public FacebookInovar getFacebookInovar() {
		return facebookInovar;
	}

	/**
	 * @param facebookInovar: o facebookInovar que sera definido
	 */
	public void setFacebookInovar(FacebookInovar facebookInovar) {
		this.facebookInovar = facebookInovar;
	}
	
	
	
}
