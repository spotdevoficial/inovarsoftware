/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.model.business.FacebookInovarBusiness;
import br.com.spotdev.inovarsoftware.model.business.MessageToSendBusiness;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.model.value.MessageToSend;
import br.com.spotdev.inovarsoftware.view.SendMessageItemView;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.Group;
import facebook4j.GroupMember;
import facebook4j.Reading;
import facebook4j.ResponseList;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * @author Davi Salles
 * @description Controle da navbar de envio de mensagens
 * localizada a direita na HomeController 
 *
 */
public class SendMessagesController {
	
	/**
	 * Thread que esta sendo usada para enviar mensagens
	 */
	private Thread threadEnvio;
	
	/**
	 * HomeController a quem esse controller pertence
	 */
	private HomeController controller;
	
	/**
	 * Componente do botão de iniciar o envio de mensagens
	 */
	private Button buttonEnviar;
	
	/**
	 * Botão de limpar mensagens no canto superior direito
	 */
	private Button buttonLimparMensagens;
	
	/**
	 * Painel que contem todas as mensagens
	 * ja enviadas
	 */
	private VBox paneForMessages;
	
	/**
	 * Conta do facebook que esta conectada 
	 */
	private FacebookInovar facebookInovar;
	
	/**
	 * Tela do carregamento das mensagens
	 */
	public static SendingMessageController SENDING_MSG_CONTROLER;
	
	public SendMessagesController(HomeController controller){
		
		this.controller = controller;
		this.buttonEnviar = controller.buttonEnviar;
		this.paneForMessages = controller.paneForMessages;
		this.buttonLimparMensagens = controller.buttonLimparMensagens;
		
		if(!InovarSoftware.DISABLE_FACEBOOK){
			FacebookInovar facebookInovar = controller.getFacebookInovar();
			this.facebookInovar = facebookInovar;
		}
		
		// Adiciona alguns listeners
		buttonEnviar.setOnAction(this::onClickEnviarEvent);
		buttonLimparMensagens.setOnAction(this::onClickButtonLimparMensagens);
		
	}

	/**
	 * Evento chamado quando o botão Enviar no canto inferior direito
	 * da tela Home é pressionado
	 * 
	 * @param e Informações do evento
	 * 
	 * TODO Usar apenas uma webview para toda a fila
	 */
	public void onClickEnviarEvent(ActionEvent e){
		
		if(!InovarSoftware.DISABLE_FACEBOOK){
			
			// Mostra a tela de adicionar mensagens caso o usuario
			// não tenha adicionado mensagens ainda
			if(getFacebookInovar().getMessages().size() == 0){
				
				GerenciarMensagensController.
				iniciar(controller.getView().getStage());
				return;
				
			}
			
			// Mostra a tela de adicionar grupos caso o usuario não tenha
			// adicionado grupos ainda
			if(getFacebookInovar().getGroups().size() == 0){
				getController().getGroupArea().getFacebookList().showLoaded();
				return;
			}
			
			// Faz o botão ficar bloqueado 
			setButtonEnviarToParar(true);
			
			SENDING_MSG_CONTROLER = SendingMessageController.iniciar(controller);
			// Loop em todos os grupos para pegar os membros
			Facebook facebook = facebookInovar.getFacebook();
			Thread t = new Thread(()-> {
				for(Group g : facebookInovar.getGroups()){
					
					try{
						// Loop em todos os membros para enviar a mensagem
						ResponseList<GroupMember> members = 
								facebook.getGroupMembers(g.getId(),new Reading().
										limit(InovarSoftware.LIMIT_MEMBERS));
						int atual = SENDING_MSG_CONTROLER.getMaxUsers();
						SENDING_MSG_CONTROLER.setMaxUsers(atual+members.size());
						
						for(GroupMember gm : members){
							String name = gm.getName();
							String id = gm.getId();
													
							// Adiciona o usuario na fila para receber a mensagem
							String message = 
									FacebookInovarBusiness.
									getRandomMessage(facebookInovar);
							
							// Da um replace nas tags da mensagem
							message = message.replace("{nome}", name);
							message = message.replace("{primeiro_nome}", 
									name.split(" ")[0]);
							message = message.replace("{ultimo_nome}", 
									name.split(" ")[name.split(" ").length-1]);
							
							MessageToSend mst = new MessageToSend(message, 
									id, facebookInovar, name);
								
							try{
								MessageToSendBusiness.sendMessage(mst);
								onSentMessage(mst, new Date());
							}catch(Exception ex){
								ex.printStackTrace();
							}
	
						}
						
					}catch(Exception ex){
						ex.printStackTrace();
					}
					
				}
				
				onTerminouDeEnviarMensagens();
			}, "Thread para envio de mensagens");
			setThreadEnvio(t);
			t.start();
			
			
		}else{
			setButtonEnviarToParar(true);
			SendMessageItemView smiv = new SendMessageItemView(
					"http://i.ytimg.com/vi/sXOdn6vLCuU/maxresdefault.jpg", 
					"12:00", 
					"Olá sou davisir kkkk");
			paneForMessages.getChildren().add(smiv);
			
		}
				
	}
	
	/**
	 * Função chamada quando o botão de parar mensagens
	 * localizado no canto inferior direito da tela é
	 * pressionado
	 * 
	 * @param ae Informações do evento
	 */
	public void onClickPararMensagens(ActionEvent ae){
		
		getThreadEnvio().interrupt();
		setButtonEnviarToParar(false);
		
	}
	
	/**
	 * Função chamada quando uma mensagem é enviada com sucesso
	 * 
	 * @param mts Mensagem que foi enviada
	 * @param d Momento em que foi enviada
	 */
	public void onSentMessage(MessageToSend mts, Date d){
		
		SendingMessageController smc = SENDING_MSG_CONTROLER;
		smc.addUserAoContador();
		
		String url = "http://www.juliejohnsonlaw.com/wp-content/uploads/2014/10/blank-person-photo-.png";
		try {
			url = getFacebookInovar().getFacebook().getPictureURL(mts.getToId()).toString();
		} catch (FacebookException e) {
			e.printStackTrace();
		}
		
		// Adiciona o item da mensagem
		SendMessageItemView smiv = new SendMessageItemView(url, new SimpleDateFormat("HH:mm").format(d), mts.getMessage());
		Platform.runLater(()->paneForMessages.getChildren().add(smiv));
		
	}
	
	/**
	 * Evento chamado quando todas as mensagnes foram enviadas
	 */
	public void onTerminouDeEnviarMensagens(){
		
		setButtonEnviarToParar(false);
		
	}
	
	/**
	 * Tranforma o buttonEnviar de PARAR para ENVIAR e vice-versa
	 * 
	 * @param transformToParar 
	 * 	caso TRUE o botão ira virar PARAR
	 * 	caso FASLE o botão ira virar ENVIAR
	 */
	public void setButtonEnviarToParar(boolean transformToParar){
		
		Platform.runLater(()-> {
			if(transformToParar){
				buttonEnviar.setText("Parar");
				buttonEnviar.setOnAction(this::onClickPararMensagens);
			}else{
				buttonEnviar.setText("Enviar");
				buttonEnviar.setOnAction(this::onClickEnviarEvent);
			}
		});
	}
	
	/**
	 * Remove todas as mensagens listadas
	 * enviadas nos paineis do canto direito
	 * da tela
	 */
	public void onClickButtonLimparMensagens(ActionEvent ae){
		paneForMessages.getChildren().clear();
	}
	
	/**
	 * @return valor do controller
	 */
	public HomeController getController() {
		return controller;
	}

	/**
	 * @return valor do buttonEnviar
	 */
	public Button getButtonEnviar() {
		return buttonEnviar;
	}

	/**
	 * @return o threadEnvio
	 */
	public Thread getThreadEnvio() {
		return threadEnvio;
	}

	/**
	 * @param threadEnvio é o threadEnvio a ser definido
	 */
	public void setThreadEnvio(Thread threadEnvio) {
		this.threadEnvio = threadEnvio;
	}

	/**
	 * @return valor do facebookInovar
	 */
	public FacebookInovar getFacebookInovar() {
		return facebookInovar;
	}

	/**
	 * @param facebookInovar: o facebookInovar que sera definido
	 */
	public void setFacebookInovar(FacebookInovar facebookInovar) {
		this.facebookInovar = facebookInovar;
	}
	
}
