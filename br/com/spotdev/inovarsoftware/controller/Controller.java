/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.net.URL;
import java.util.ResourceBundle;

import br.com.spotdev.inovarsoftware.view.View;
import javafx.fxml.Initializable;
/**
 * @author Davi Salles
 * @description Classe que representa os controllers
 */
public abstract class Controller implements Initializable {

	/**
	 * View principal do controller
	 */
	private View view = null;
	
	/**
	 * @return valor do view
	 */
	public View getView() {
		return view;
	}
	
	/**
	 * Função chamada depois que o stage do controller é criado
	 */
	public abstract void beforeCreateStage();

	/**
	 * @param view: o view que sera definido
	 */
	public void setView(View view) {
		this.view = view;
	}

	/*
	 * @description Função da calase Initilizable
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
	}

}
