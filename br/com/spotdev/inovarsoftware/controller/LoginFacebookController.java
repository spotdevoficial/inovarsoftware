/**
 * 
 */
package br.com.spotdev.inovarsoftware.controller;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookieStore;

import br.com.spotdev.inovarsoftware.InovarSoftware;
import br.com.spotdev.inovarsoftware.model.business.FacebookInovarBusiness;
import br.com.spotdev.inovarsoftware.model.value.FacebookInovar;
import br.com.spotdev.inovarsoftware.view.LoginFacebookView;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeOutRightTransition;
import br.com.spotdev.inovarsoftware.view.utils.animations.FadeInRightTransition;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Davi Salles
 * @description Controller da tela de logar no facebook
 */
public class LoginFacebookController extends Controller {

	/**
	 * Procedimento para carregar o controller
	 * 
	 * @param stage
	 *            em que o controller esta sendo carregado
	 */
	public static void iniciar(Stage stage) {

		LoginFacebookView loginView = new LoginFacebookView();
		try {
			loginView.carregarEMostrar(stage);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Mostra o pane de loading
	 * 
	 * @param runnable
	 *            Interface de ação que rodará depois do pane ter carregado
	 */
	public void changeToLogin(Runnable runnable) {

		FadeOutRightTransition animaOut = 
				new FadeOutRightTransition(loadingPane);
		animaOut.play();
		animaOut.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				loadingPane.setVisible(false);

				FadeInRightTransition animaOut = 
						new FadeInRightTransition(loadingPane);
				animaOut.setDelay(new Duration(0));
				animaOut.playFromStart();
				new Thread(new Runnable() {

					@Override
					public void run() {
						Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								webView.setVisible(true);
							}
						});
							
						if(runnable != null)
							runnable.run();

					}
				}).start();

			}
		});

	}
	
	/**
	 * Evento chamado quando o stage é criado
	 */
	@Override
	public void beforeCreateStage(){
		
		// Cria uma conta do facebook da inovar
		FacebookInovar fi = FacebookInovarBusiness.instanceFacebookAccount();
		Facebook facebook = fi.getFacebook();

		// Mostra a tela de login para o facebook
		WebEngine wg = webView.getEngine();
		changeToLogin(null);
		wg.getLoadWorker().stateProperty().
		addListener( new ChangeListener<State>() {
            public void changed(
            		@SuppressWarnings("rawtypes") ObservableValue ov, 
            		State oldState, 
            		State newState) {
               
            	// Verifica a URL da tela
            	String url = wg.getLocation();
            	// Verifica se foi possível logar com sucesso
            	if (newState == State.SUCCEEDED && url.contains("code=")) {
					String accessToken = FacebookInovarBusiness.
							getCodeAcess(url);
					
					if(accessToken.equals("200")){
						getView().getStage().close();
						return;
					}
                	
                	// Faz o login na intancia
                	try {
						facebook.setOAuthAccessToken(facebook.
								getOAuthAccessToken(accessToken));
					} catch (FacebookException e) {
						e.printStackTrace();
					}
                	
                	// Fecha a tela de login
                	getView().getStage().close();
                	HomeController.MAIN_INSTANCE.permitirInteracao(true, false);
                	HomeController.MAIN_INSTANCE.loadFacebookAccount(fi);
                	wg.getLoadWorker().stateProperty().removeListener(this);
                	
                	// Define a thread Id do usuario
                	CookieManager manager = InovarSoftware.COOKIE_MANAGER;
                	CookieStore cookieJar =  manager.getCookieStore();
                    cookieJar.getCookies().parallelStream().
	                	filter(c->c.getName().equals("c_user")).
	                	forEach(c->fi.setThreadId(c.getValue()));               	
						
            	
                }
            }
        });
		
		// Carrega a tela de login
		wg.load(fi.getLoginUrl());
		
	}

	/**
	 * Pane principal da tela de loading
	 */
	@FXML
	public AnchorPane loadingPane;

	/**
	 * Webview para acessar o facebook
	 */
	@FXML
	public WebView webView;

}
