package br.com.spotdev.inovarsoftware.utils;

import java.util.regex.Matcher;

public interface StringReplacerCallback {
    public String replace(Matcher match);
}