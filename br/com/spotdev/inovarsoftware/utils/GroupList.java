/**
 * 
 */
package br.com.spotdev.inovarsoftware.utils;

import java.util.ArrayList;
import java.util.LinkedList;

import facebook4j.Group;

/**
 * @author Davi Salles
 * @description
 */
public class GroupList extends LinkedList<Group> {

	/**
	 * Serial version UID do objeto
	 */
	private static final long serialVersionUID = 6070818606621400446L;

	/**
	 * Evento que será chamado qua
	 */
	private ArrayList<GroupEvent> onAddItem = new ArrayList<>();
	
	/**
	 * Evento que será chamado quando remover item
	 */
	private ArrayList<GroupEvent> onRemoveItem = new ArrayList<>();
	
	/**
	 * Função que deve ser usada para chamar 
	 * todos os eventos do additem
	 */
	private void callOnAddItem(Group item){
		
		Iterable<GroupEvent> iterable = onAddItem;
	    for (GroupEvent r : iterable) {
	        r.handle(item);
	    }
		
	}
	
	/**
	 * Função que deve ser usada para chamar 
	 * todos os eventos do onRemoveItem
	 */
	private void callOnRemoveItem(Group item){
		
		Iterable<GroupEvent> iterable = onRemoveItem;
	    for (GroupEvent r : iterable) {
	        r.handle(item);
	    }
		
	}

	/* (non-Javadoc)
	 * @see java.util.LinkedList#addFirst(java.lang.Object)
	 */
	@Override
	public void addFirst(Group e) {
		callOnAddItem(e);
		super.addFirst(e);
	}
	
	/* (non-Javadoc)
	 * @see java.util.LinkedList#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		Group item = (Group)o;
		callOnRemoveItem(item);
		return super.remove(o);
	}

	/**
	 * Adiciona um eventode quando um item da lista é
	 * adicionado com addFirst();
	 * 
	 * @param r evento do item
	 */
	public void addOnPutItemEvent(GroupEvent r){
		onAddItem.add(r);
	}
	
	/**
	 * Adiciona um evento de quando um item da lista é
	 * removido com remove();
	 * 
	 * @param r evento do item
	 */
	public void addOnRemoveItemEvent(GroupEvent r){
		onRemoveItem.add(r);
	}
	
	public interface GroupEvent {
		
		public void handle(Group group);
	}
	
	@Override
	public void clear(){
		for(Group g : this){
			callOnRemoveItem(g);
		}
		
		super.clear();
	}
	
}
