package br.com.spotdev.inovarsoftware.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import br.com.spotdev.inovarsoftware.controller.HomeController;
import facebook4j.internal.org.json.JSONObject;

public class MessageUtils {

	/**
	 * Variáveis do final do post
	 */
	private static String DEFAULT_MERGE = null;
	
	/**
	 * Função que transforma binario em decimal para gerar hashs
	 * 
	 * @param data Informação que virará decimal
	 * @return Retorna a data transformada em decimaç
	 */
	public static String binaryToDecimal(String data) {
		String ret = "";
		while (!data.equals("0")) {
			
			int end = 0;
			String fullName = "";
			int i = 0;
			
			for (;i < data.length();i++) {
				
				end = 2 * end + Integer.parseInt(data.split("")[i], 10);
				if (end >= 10) {
					fullName += "1";
					end -= 10;
				} else {
					fullName += "0";
				}
				
			}
			
			ret = Integer.toString(end) + ret;
			int index = fullName.indexOf("1");
			if(index != -1)
				data = fullName.substring(index);
			else
				data = "0";
		}
		return ret;
	}
	
	/**
	 * Retorna os últimos dados necessarios para o chat
	 * 
	 * @param html Página HTML que irá ser usada
	 * @param userId ID do user da página
	 * @return Retorna as últimas variaveis necessárias para o post
	 */
	public static String makeMergeWithDefaults(String html, String userId) {
		
		if(DEFAULT_MERGE != null)
			return DEFAULT_MERGE;
		
		int reqCounter = 1;
		String fb_dtsg = getFrom(html, "name=\"fb_dtsg\" value=\"", "\"");
		String ttstamp = "";
		
		for (int i = 0; i < fb_dtsg.length(); i++) {
			ttstamp += Character.codePointAt(fb_dtsg,i);
		}
		
		ttstamp += '2';
		String keyEnd = "&__user="+userId+
			"&__a=1"+
			"&__dyn=aKTyduy9k9FoAyQ449VUOWEyAy94uezkHyUmyVbGAGVozBDirWWGm5popy5AKGzonAyF4rCyp68Q4WCxOfUSq6UC58K8-qp2axdi2eqiHxt78EWVp5qXDmcCK5p8zADCxO"+
			"&__req="+Integer.toString(++reqCounter,36)+
			"&fb_dtsg="+fb_dtsg+
			"&ttstamp="+ttstamp+
			"&__rev="+getFrom(html, "revision\":",",");
		
		DEFAULT_MERGE = keyEnd;
		return keyEnd;
	}

	/**
	 * Retorna uma página HTML do usuario
	 * 
	 * @param id Id do usuario que irá pegar a página html
	 * @return Retorna todo html da página
	 */
	public static String getMyPageHtml(){
		
		try{
			URL obj = new URL("https://www.facebook.com/");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	
			//add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			con.setRequestProperty("Referer", "https://www.facebook.com/");
			con.setRequestProperty("Host", "www.facebook.com");
			con.setRequestProperty("Origin", "https://www.facebook.com");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18");
			con.setRequestProperty("Connection", "keep-alive");
			
			// Send post request
			con.setDoOutput(true);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
			return response.toString();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	static long sectionLength;
	/**
	 * Gera o GUID para requisição do search no facebook
	 */
	public static String getGUID() {

		sectionLength = new Date().getTime();
		String id = 
				StringReplacer.replace("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx", 
						Pattern.compile("[xy]",Pattern.DOTALL), (Matcher m) -> {
			long r = (long)Math.floor((sectionLength + Math.random() * 16) % 16);
			sectionLength = (long)Math.floor(sectionLength / 16);
			String _guid = Long.toString(m.group() == "x" ? r : r & 7 | 8,16);
			return _guid;
		});
		return id;
	}
	
	
	/**
	 * Retorna uma hash para o post da mensagem
	 * 
	 * @param str String a ser encontrada
	 * @param startToken Posição da string procurada
	 * @param endToken Final da posição
	 * @return Retorna uma hash com um código
	 */
	public static String getFrom(String str, String startToken, 
			String endToken) {
	  int start = str.indexOf(startToken) + startToken.length();
	  if(start < startToken.length()) return "";

	  String lastHalf = str.substring(start);
	  int end = lastHalf.indexOf(endToken);
	  return lastHalf.substring(0, end);
	}
	
	public static String getUserId(String name) throws UnsupportedEncodingException {
		
		String myId = 
				HomeController.MAIN_INSTANCE.getFacebookInovar().getThreadId();
	    String form = 
	      "value="+URLEncoder.encode(name.toLowerCase(),"UTF-8")+
	      "&viewer="+myId+
	      "&rsp=search"+
	      "&context=search"+
	      "&path="+URLEncoder.encode("/home.php","UTF-8")+
	      "&request_id="+URLEncoder.encode(getGUID(),"UTF-8");
	    form+=makeMergeWithDefaults(getMyPageHtml(), myId);
	    	    
	    try{
			URL obj = new URL("https://www.facebook.com/ajax/typeahead/search.php?"+form);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	
			//add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
			con.setRequestProperty("Referer", "https://www.facebook.com/");
			con.setRequestProperty("Host", "www.facebook.com");
			con.setRequestProperty("Origin", "https://www.facebook.com");
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18");
			con.setRequestProperty("Connection", "keep-alive");
			
			// Send post request
			con.setDoOutput(true);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
			String jsonData = response.toString().replaceFirst(
					Pattern.quote("for (;;);"), "");
			JSONObject json = new JSONObject(jsonData);
			String jarray = json.getJSONObject("payload").
					getJSONArray("entries").
					getJSONObject(0).getString("uid");
			return jarray;
		}catch(Exception e){
			e.printStackTrace();
		}
	    
	    return null;
	}
	
}
